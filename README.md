# Laboratoires ASP - SD Card

Auteur : Bruno Da Rocha Carvalho & Nils Basset
Date : 13.06.2021

## Fonction bas niveau

### Étape 2

Nous avons trouvé au chapitre 4.7.4 les deux commandes suivantes pour nous permettre de lire les registres CID et CSD : 

| CMD index | abréviation | description                                              |
| --------- | ----------- | -------------------------------------------------------- |
| CMD9      | SEND_CSD    | La carte envoie ces CSD (card specific data) sur le CMD  |
| CMD10     | SEND_CID    | La carte envoie ces CID (card identification) sur le CMD |

Nous avons donc pu compléter la fonction sdhc_init() de sorte à stockez les valeurs de ces registrer dans des variables global : `cid_reg[4]` et `csd_reg[4]`.

Nous avons donc exécuté la commande 10 pour récupérer les 128 bits du registre CID `mmchs_send_command((ulong) 10, arg, 0, 0);`. Puis récupéré ces informations avec les registres suggéré dans la données :

```c
    cid_reg[0] = MMCHS1_REG(MMCHS_RSP10); // Bits 31 à 0
    cid_reg[1] = MMCHS1_REG(MMCHS_RSP32); // Bits 63 à 32
    cid_reg[2] = MMCHS1_REG(MMCHS_RSP54); // Bits 95 à 64
    cid_reg[3] = MMCHS1_REG(MMCHS_RSP76); // Bits 127 à 96
```

Et de même pour les 128 bits du registre CSD , mais avec la commande 9.

```c
mmchs_send_command((ulong) 9, arg, 0, 0);
    csd_reg[0] = MMCHS1_REG(MMCHS_RSP10); // Bits 31 à 0
    csd_reg[1] = MMCHS1_REG(MMCHS_RSP32); // Bits 63 à 32
    csd_reg[2] = MMCHS1_REG(MMCHS_RSP54); // Bits 95 à 64
    csd_reg[3] = MMCHS1_REG(MMCHS_RSP76); // Bits 127 à 96
```

Ensuite pour pouvoir lire le product name (PNM), qui est stocké dans les bits 103 à 64 du registres CID.
Nous devons donc jouer entre les valeur stockées dans cid_reg[3]` et `cid_reg[2]` pour récupéré toute l'information.

```c
void read_productname(uchar * name)
{
    //On récupére caractère par caractère (8 bits par 8 bits)
    name[0]= cid_reg[3] & 0xFF;
    name[1]= (cid_reg[2] >> 24) & 0xFF;
    name[2]= (cid_reg[2] >> 16) & 0xFF;
    name[3]= (cid_reg[2] >> 8) & 0xFF;
    name[4]= cid_reg[2] & 0xFF;
    name[5]= '\0';
}
```

Nous récupérons chaque caractère stocké entre les bits 103 et 64, soit :

- Caractère 1 : 103 à 96 
- Caractère 2 : 95 à 88
- Caractère 3 : 87 à 80 
- Caractère 4 : 79 à 72
- Caractère 5 : 71 à 64

Pour former une chaîne de caractère que nous terminons par un '\0' pour en détecter la fin facilement.

Finalement pour pouvoir lire la taille de stockage de la carte (C_SIZE), qui est stocké dans les bits 69 à 48 du registres CSD. Nous avons donc jouer entre les valeur stockées dans csd_reg[2]` et `csd_reg[1]` pour récupérer toute l'information. Et utilisé la formule fournie afin de connaître la capacité totale :  capacité totale = ((C_SIZE + 1) * 512) Kbytes 

```c
ulong read_card_size()
{
    ulong size = 0;
    //chap. 5.3.3 / device size / C_SIZE / 22 bits / [69:48]
    size = ((csd_reg[2] & 0x3F) << 16) | (csd_reg[1] >> 16);

    return ((size + 1) * 512);
}
```

### Étape 3

Nous avons implémenté la fonction `mmchs_read_block(ulong *data, ulong block)` en se basant sur la figure 24-39. Nous avons donc fait comme expliqué :

1. nous modifions le registre MMCHS_STAT :`MMCHS1_REG(MMCHS_STAT) = 0x18;` qui nous permet de clear le status des deux module BWR et BRR
2. Ensuite nous attendons en observant le bit 2 du registre PSTATE qu'il soit égale à 0. Cela nous permet de savoir que nous pouvons utiliser les commande (Issuing of command using the mmci_dat lines is allowed).
3. Nous activons le mode BRR (mode read) et désactivons le mode BWR (mode write) avec le registre MMCHS_IE
   `MMCHS1_REG(MMCHS_IE) |= MMCHS_IE_BRR_ENABLE; MMCHS1_REG(MMCHS_IE) &= ~MMCHS_IE_BWR_ENABLE;`
4. Nous exécutons la commande n°17 "READ_SINGLE_BLOCK" `mmchs_send_command((ulong) 17, block, 0, 1);`
5. On attends que le registre de statut MMCHS_STAT nous disent qu'il est prêt à lire le buffer `while (!(MMCHS1_REG(MMCHS_STAT) & MMCHS_STAT_BRR));`
6. On clear le bit précédemment activé pour que dans le futur une nouvelle interruption puisse avoir lieu `MMCHS1_REG(MMCHS_STAT) = MMCHS_STAT_BRR;`
7. On lit le registre data et le stockons dans notre buffer
8. Nous attendons que le bit du registre status MMCHS_STAT correspondant au "Transfer Completed" s'active. Car en effet il s'active automatiquement à la fin de la lecture `while ((MMCHS1_REG(MMCHS_STAT) & MMCHS_STAT_TC) == 0);`
9. Pour ainsi le désactiver `MMCHS1_REG(MMCHS_STAT) = MMCHS_STAT_TC;`

Nous avons donc pu observer ce que contenais les quelques premier block de donnée : 

Dans le block 0 on retrouve une séquence tel que : [50462976, 117835012, ..., 4294901244, 50462976, 117835012, ..., 4294901244] qui transformé en hexadécimal nous donne [0x030201000, 0x07060504, ..., 0xFFFEFDFC,..] On retrouve donc dans se premier block une énumération de 0 à 255 deux fois (0 à ff).

Dans le block 1 on retrouve deux nombres qui se répète : [19088743, 2303737967,..] qui transformé en hexadécimal nous donne [0x1234567, 0x8950406F]. Ne fais pas vraiment sens..

Dans le block 2 on trouve un seul nombre qui se répète : [3405691582, ..] qui transformé en hexadécimal nous donne : [0xCAFEBABE]

Et enfin dans le block 3 on trouve également un seul nombre qui se répète : [3735928495, ..] qui transformé en hexadécimal nous donne [0xDEADBEAF, ..].

### Étape 4

Nous avons implémenté la fonction `mmchs_read_multiple_block(uchar *data, ulong block, uchar nblocks)` afin de pouvoir lire plusieurs block d'un coup.

La fonction fonctionne fondamentalement comme pour lire un seul block sauf que nous avons créer un buffer dans lequel on stock, pas à pas, nos block avant de les copier dans la variables donné en paramètres.

Nous avons pu vérifier sont fonctionnement avec les valeur ressorties dans le point précédent et voyant donc que notre implémentation fonctionne.

### Étape 5 et 6

Nous avons implémenté les fonction `mmchs_write_block(const vulong *data, ulong block)` et `mmchs_write_multiple_block(const uchar *data, ulong block, uchar nblocks)`. Elles fonctionnent exactement comme pour les fonctions de lecture, a contrario que cette fois-ci nous activons le mode BWR et désactivons le mode BRR.

## Moniteur

### Étape 1

Nous l'avons implémentée mais elle ne fonctionne pas. Nous n'avons pas réussi a la débuguée ni a comprendre pourquoi elle ne fonctionnait pas.

### Étape 2

Nous avons réussi rapidement a comprendre comment mettre en place la fonction `read_files`. Néanmoins la gestion des retours à la ligne ne fonctionne pas / pas bien

### Étape 3

Nous avons eu vraiment de la peine, entre autre a débuguer le programme. En effet les temps d'attentes entre chaque sauts, l'IDE qui plante etc etc.. on rendu cette étape laborieuse, jusqu'à une scénario parfait ou manuellement nous avons réussi a exécuté notre programme pas à pas et créer un fichier sur la carte SD.
Ce qui nous as permis de nous rendre compte que nous aurions peut-être besoin d'un délai quelque part dans notre programme. En fouillant le code founis nous avons pu trouver la présence d'une attente `wait(10000)` placée juste après un clear du registre "MMCHS_STAT".
Nous avons donc ajouté cette attente dans chacune de nos fonctions et nous avons réussi à la faire fonctionner pour des écritures **ne dépassant pas 1 block**.

Lorsque l'on a plusieurs block le programme plante dans la fonction `mmchs_send_command` au niveau de : `while ((MMCHS1_REG(MMCHS_STAT) & MMCHS_STAT_CC)==0);`. Nous n'avons pas trouvé de moyen de réglé ce problème.

### Étape 4

Pas implémenté

