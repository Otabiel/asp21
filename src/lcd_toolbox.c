/* ----------------------------------------------------------------------
   Institut ReDS - HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Evangelina LOLIVIER-EXLER
   Email  : evangelina.lolivier-exler@heig-vd.ch
   Date   : 17/09/2008
   Modified: 24/05/2012 REPTAR board adaptation
   Description : LCD toolbox
   -----------------------------------------------------------------------
*/

#include "lcd_toolbox.h"
#include "lcd.h"
#include "vga_lookup.h"
#include "fb_fonts.h"
#include "gpio.h"
#include "prcm.h"
#include "bits.h"
#include <stdio.h>

#define LINES_PER_SCREEN        (PIXELS_PER_COL/FONT_HEIGHT)
#define MAX_CHAR_PRINT_STRING 100


// globals to keep track of foreground, background colors and x,y position
uchar lcd_color_depth=16;       // 16 bits per pixel
uchar lcd_bg_color=3;           // 0 to 15, used as lookup into VGA color table
uchar lcd_fg_color=8;           // 0 to 15, used as lookup into VGA color table



//------------------------------------------------------------------------------
// Delay for some usecs. - Not accurate
// and no Cache
void udelay(int delay)
{
    volatile int i;
    for ( i = LOOPS_PER_USEC * delay; i ; i--)
        ;
}



///--------------------------------------------------------------------------
// lcd_on
//
// This function turns on the DM3730 Display Controller
//
void lcd_on()
{

    // Enable the clocks to the DISPC
    // enable functional clock (will be enable with the LCD_on function)
    DSS_CM_REG(CM_FCLKEN)|=BIT0;
    // enable interfaces clocks (L3 and L4)  (will be enable with the LCD_on function)
    DSS_CM_REG(CM_ICLKEN)|=BIT0;

    // power up the LCD
    udelay(10000);
    // LCD output enable
    LCD_REG(control)|= BIT0;
    udelay(10000);

}

//--------------------------------------------------------------------------
// lcd_off
//
// This function turns off the DM3730 Display Controller
//
void lcd_off()
{
    // power down the LCD
    udelay(10000);

    // digital output disable
    LCD_REG(control)&=~BIT1;
    // LCD output disable
    LCD_REG(control)&=~BIT0;
    udelay(10000);
}




// ------------------------------------------------------------------
// get_pixel_add return the address of a pixel with respect to the frame buffer start address
ulong* get_pixel_add (ulong X, ulong Y) //J'ai passé le retour de fonction en ulong* au lieu de ulong pour pouvoir retourner un ptr
{
    return (ulong*)FRAME_BUFFER_ADDR + ((PIXELS_PER_ROW * Y) + X);
}

// ------------------------------------------------------------------
// fb_set_pixel sets a pixel to the specified color (between 0 et 15).
void fb_set_pixel(ulong X, ulong Y, uchar color)
{
    ulong* addrPixel = get_pixel_add(X,Y); //Récupération de l'adresse pixel
    *addrPixel = vga_lookup[color]; //Envoi du code couleur (2bytes) à l'adresse du pixel
}



// ------------------------------------------------------------------
// fb_print_char prints a character at the specified location.
//
void fb_print_char(uchar cchar, ulong x, ulong y, uchar color)
{
    uchar rowChar; //Code binaire pour une ligne d'un char

    for(int i = 0; i < FONT_HEIGHT; ++i){
        rowChar = fb_font_data[cchar - FIRST_CHAR][i + 1]; //Acquisition code binaire par ligne
        for(int j = 0; j < FONT_WIDTH; ++j){
            if(rowChar & (0x1 << (FONT_WIDTH - j - 1))){ //Décalage des bits depuis le MSB pour afficher le char dans le bon sens
                fb_set_pixel(x + j, y + i, color);
            }
        }
    }
}

// ------------------------------------------------------------------
// fb_print_string prints a string at the specified location.(30 characters max)
//
void fb_print_string(uchar *pcbuffer, ulong x, ulong y, uchar color)
{
    int i = 0;
    while(pcbuffer[i] != '\0'){
        if(*(pcbuffer + i) == 13 || x == PIXELS_PER_ROW){
            line_nb++;
            y = get_current_y(line_nb);
        }
        fb_print_char(pcbuffer[i], x + i*FONT_WIDTH, y, color); //Décale le X de la taille d'un char

        i++;

        if(pcbuffer[i] == '\0' || i > 2000) //Sort de la boucle si le string est fini
            break;
    }
}

// ------------------------------------------------------------------
//
// This function clear the screen
//
void clear_screen()
{
    for(ulong i = 0; i < PIXELS_PER_ROW; ++i){
        for(ulong j = 0; j < PIXELS_PER_COL; ++j){
            fb_set_pixel(i, j, 8); //Set tout l'écran en blanc
        }
    }
}
