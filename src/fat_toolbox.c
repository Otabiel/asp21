/*
 * fat_toolbox.c
 *
 *  Created on: 12 nov. 2012
 *      Author: Evangelina Lolivier-Exler
 */

#include "stddefs.h"
#include "ff.h"
#include "lcd_toolbox.h"
#include "lcd.h"
#include "fat_toolbox.h"
#include <string.h>
#include <stdio.h>

/* data buffer, size : 10 clusters, 1 cluster = 8 blocks of 512 B */
uchar buff[MAX_FILE_SIZE];

int create_file(char *file_name, uchar *data, ulong nbyte)
{

    FIL file;

    WORD nbBlockToWrite = (nbyte / 513) + 1;
    WORD bytesWrite = 0;
    WORD totBytes = 0;
    WORD nbBytes;

    if(nbBlockToWrite > 8){
        return 1;
    }
    FRESULT result;
    result = f_open(&file,(const char*) file_name, FA_CREATE_ALWAYS | FA_WRITE);

    if(result != FR_OK){
        return result;
    }

    while(1){
        nbBytes = (nbyte > 512) ? 512 : nbyte;
        result = f_write(&file, data + totBytes, nbBytes, &bytesWrite);

        if(result != FR_OK){
            f_close(&file);
            return result;
        }

        totBytes += bytesWrite;
        nbBlockToWrite--;

        if(nbBlockToWrite <= 0){
            break;
        }
        nbyte -= nbBytes;

    }
    f_close(&file);
    return 0;
}

/* path is "" for the root directory */
void scan_files (char *path)
{
    /* Completer le code ici */
    clear_screen();
    FILINFO finfo;
    DIR dirs;
    int i;

    if (f_opendir(&dirs, path) == FR_OK) {
        i = strlen(path);
        while ((f_readdir(&dirs, &finfo) == FR_OK) && finfo.fname[0]) {
            if (finfo.fattrib & AM_DIR) {
                sprintf(path+i, "/%s", &finfo.fname[0]);
                scan_files(path);
                *(path+i) = '\0';
            } else {
                sprintf((char*)buff, "%s/%s\n", path, &finfo.fname[0]);
                fb_print_string(buff, 0, get_current_y(line_nb), lcd_fg_color);
                line_nb++;
            }
        }
    }
}

int read_file(char *file_name)
{

    clear_screen();
    FIL file;

    WORD totBytesRead = 0;
    WORD bytesRead;

    FRESULT result;
    result = f_open(&file,(const char*) file_name, FA_OPEN_EXISTING | FA_READ);

    if(result != FR_OK){
        return result;
    }

    while(1){
        result = f_read(&file, buff + totBytesRead, 512, &bytesRead);
        if(result != FR_OK){
            f_close(&file);
            return result;
        }

        fb_print_string(buff, 0, get_current_y(line_nb), lcd_fg_color);
        line_nb++;

        if(bytesRead < 512){
            break;
        }
    }
    f_close(&file);

    return 0;
}

int print_file_info(char *file_name)
{
    /* Completer le code ici */
    clear_screen();

    FILINFO finfo;

    char line[100];
    FRESULT result = f_stat(file_name, &finfo);

    if(result){
        return result;
    }

    //sprintf((char *)line, "File name: %s\rSize: %d\rDate: %d",&finfo.fname[0], finfo.fsize, finfo.fdate);
    //fb_print_string(line,0, get_current_y(line_nb), lcd_fg_color);


    return 0;
}
