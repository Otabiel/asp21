/*
 * application.c
 *
 *  Created on: Apr 24, 2021
 *      Author: reds
 */
#include "stdio.h"
#include "application.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "bits.h"
#include "timer_toolbox.h"
#include "lcd.h"
#include "gpio.h"

#define MASK_SW2    BIT7
#define MASK_LED0   0x8000
#define GPIO5       5
#define GPIO6       6

int gameON;
int gameEnded;
int score1;
int score2;
int bestTime;

void gameInit(){
    gameON = 0;
    gameEnded = 0;
    score1 = 0;
    score2 = 0;
    bestTime = 0;
}

int returnGameStatus(){
    return gameON;
}

void endGame(){
    gameEnded = 1;
}

void reverseGameStatus(){
    if(gameON == 1){
        gameON = 0;
    } else {
        gameON = 1;
    }
}

void gameLoop(){
    gameInit();
    start_timer();

    fb_print_string((uchar*)"Press SW2 to start the game", 0, 0, 0);

    while(!ReadInput(6, MASK_SW2));

    do{
        clear_screen_zone(0, 0, PIXELS_PER_ROW, 0+16);

        displayPlayers();
        displayBestTime();

        reverseGameStatus();

        int random = (read_timer_value() % 1000);

        fb_print_string((uchar*)"Waiting for LED0...", 0, 0, 0);

        waitGame(random);

        SetOutput(GPIO5, MASK_LED0);
        write_timer_value(0x00000000);

        clear_screen_zone(0, 0, PIXELS_PER_ROW, 0+16);
        while(GPIO5_REG(OMAP_GPIO_DATAOUT)&MASK_LED0);

        waitGame(60);

        reverseGameStatus();

        gameEnded = 0;

        fb_print_string((uchar*)"SW2 to restart, SW0 to quit", 0, 80, 0);
        write_timer_value(0x00000000);

        while(!ReadInput(6, MASK_SW2));

        clear_screen_zone(0, 0, PIXELS_PER_ROW, 80 + 16);
    }while(gameEnded == 0);

    fb_print_string((uchar*)"Merci d'avoir joue", 150, 80, 0);

}

void waitGame(int delay){
    int i,j;

    for(i = 0; i < delay; ++i){
        for(j = 0; j < 1000000; j++){}
    }

    return;
}

void scoring(int speed, int joueur){
    if(!(GPIO5_REG(OMAP_GPIO_DATAOUT)&MASK_LED0)){
        decreaseScore(joueur);
        displayPlayers();
    } else {
        setBestTime(speed);
        displayWinner(speed, joueur);
        increaseScore(joueur);
        displayPlayers();
    }

}

void decreaseScore(int joueur){
    if(joueur == 1){
        score1--;
    } else {
        score2--;
    }
}

void increaseScore(int joueur){
    if(joueur == 1){
            score1++;
        } else {
            score2++;
        }
}

void displayWinner(int speed, int joueur){
    char str[30];

    sprintf(str, "Le joueur %d gagne", joueur);
    fb_print_string((uchar*)str, 0, 0, 0);

    sprintf(str, "Il a appuye en %d ms", speed);
    fb_print_string((uchar*)str, 0, 40, 0);

}

void displayPlayers(){
    clear_screen_zone(150, 150, PIXELS_PER_ROW, 165 + 16);
    char str[30];
    sprintf(str, "Joueur 1 : %d", score1);
    fb_print_string((uchar*)str, 150, 150, 0);

    sprintf(str, "Joueur 2 : %d", score2);
    fb_print_string((uchar*)str, 150, 165, 0);
}

void displayBestTime(int newRecord){
    clear_screen_zone(0, 190, PIXELS_PER_ROW, 190 + 16);
    char str[30];
    if(newRecord == 1){
        sprintf(str, "Nouveau Record ! %d ms", bestTime);
    } else {
        sprintf(str, "Meilleur temps : %d ms", bestTime);
    }

    fb_print_string((uchar*)str, 0, 190, 0);
}

void setBestTime(int newTime){
    if(bestTime == 0 || bestTime > newTime){
        bestTime = newTime;
        displayBestTime(1);
    }
}

