/*
 * application.h
 *
 *  Created on: Apr 24, 2021
 *      Author: reds
 */

#ifndef SRC_APPLICATION_H_
#define SRC_APPLICATION_H_

//Fonction contenant le jeu
void gameLoop();

//Fonction initialisant les variables nécessaires au bon fonctionnement du jeu
void gameInit();

//Inverse le statut du jeu de, en cours à pas en cours
void reverseGameStatus();

//Met fin au jeu
void endGame();

//Retourne le statut de la partie(donc si une partie est en cours ou si personne ne joue)
int returnGameStatus();

//Fonction d'attente fourie par David Truan
void waitGame(int delay);

//Affichage du joueur gagnant la manche actuel
void displayWinner(int speed, int joueur);

//Affiche les deux joueurs et leur score
void displayPlayers();

//Affiche le meilleur temps de la partie
void displayBestTime();

//Calcul les scores (augmente et réduit)
void scoring(int speed, int joueur);

//réduit le score du joueur
void decreaseScore(int joueur);

//augmente le score du joueur
void increaseScore(int joueur);

//Met à jour le meilleur temps de la partie
void setBestTime(int newTime);

#endif /* SRC_APPLICATION_H_ */
