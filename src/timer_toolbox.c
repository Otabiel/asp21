/*
 * timer_toolbox.c
 *
 *  Created on: Apr 23, 2021
 *      Author: reds
 */

#include "timer_toolbox.h"

void start_timer(){
    GPT1_REG(TCLR) |= BIT0;
}

void stop_timer(){
    GPT1_REG(TCLR) &= ~BIT0;
}

vulong read_timer_value(){
    return GPT1_REG(TCRR);
}

void write_timer_value(vulong data){
    GPT1_REG(TCRR) = data;
}
