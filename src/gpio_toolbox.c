/*
 * gpio_toolbox.c
 *
 *  Created on: Mar 16, 2021
 *      Author: reds
 */

#include "gpio_toolbox.h"
#include "gpio.h"


vulong getGPIOaddr(uchar module, ulong REGISTER){
    switch(module){
            case 1:
                return (GPIO_MOD1_BASE_ADD + REGISTER);
            case 2:
                return (GPIO_MOD2_BASE_ADD + REGISTER);
            case 3:
                return (GPIO_MOD3_BASE_ADD + REGISTER);
            case 4:
                return (GPIO_MOD4_BASE_ADD + REGISTER);
            case 5:
                return (GPIO_MOD5_BASE_ADD + REGISTER);
            case 6:
                return (GPIO_MOD6_BASE_ADD + REGISTER);
            default:
                return 0;
            }
}

/**
 * Activer une sortie (Allumer la LED)
 */
void SetOutput(uchar module, ulong bitmask){
    vulong* addr = (vulong *)getGPIOaddr(module, OMAP_GPIO_DATAOUT);

    *addr |= bitmask;
}

/**
 * Mettre à zéro une sortie (Si une led est allumée l'éteindre)
 */
void ClearOutput(uchar module, ulong bitmask){
    vulong* addr = (vulong *)getGPIOaddr(module, OMAP_GPIO_CLEARDATAOUT);

    *addr = bitmask;
}

/**
 * Lire l'état d'un input (switch)
 * Retourne 1 si il a été pressé 0 sinon
 */
uchar ReadInput(uchar module, ulong bitmask){
    vulong* addr = (vulong *)getGPIOaddr(module, OMAP_GPIO_DATAIN);

    vulong test = *addr;

    if(bitmask & test){
        return 1;
    } else {
        return 0;
    }

}

/**
 * Inverser l'état d'une sortie (LED)
 */
void ToggleOutput(uchar module, ulong bitmask){
    vulong* addr = (vulong *)getGPIOaddr(module, OMAP_GPIO_DATAOUT);

    *addr ^= (bitmask);
}

void UnmaskIRQ(uchar module, ulong bitmask){

}

void MaskIRQ(uchar module, ulong bitmask){

}
