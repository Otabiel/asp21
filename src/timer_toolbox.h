/*
 * timer_toolbox.h
 *
 *  Created on: Apr 23, 2021
 *      Author: reds
 */

#ifndef SRC_TIMER_TOOLBOX_H_
#define SRC_TIMER_TOOLBOX_H_

#include "timer.h"
#include "bits.h"

void start_timer();
void stop_timer();
vulong read_timer_value();
void write_timer_value(vulong data);

#endif /* SRC_TIMER_TOOLBOX_H_ */
