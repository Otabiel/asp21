/*---------------------------------------------------------------------------------------
 * But               : Petit programme pour etre charge dans le micro et affiche avec dump
 * Auteur            : Evangelina LOLIVIER-EXLER
 * Date              : 29.07.2008
 * Version           : 1.0
 * Fichier           : demo.c
 *----------------------------------------------------------------------------------------*/
#include "init.h"
#include "lcd_toolbox.h"
#include "cfg.h"
#include "bits.h"
#include "gpio.h"
#include "gpio_toolbox.h"
#include "init.h"
#include "stddefs.h"
#include "sd_toolbox.h"
#include <stdio.h>
#include "fat_toolbox.h"
#include "ff.h"
//#include "application.h"

/* Global variables */
vulong AppStack_svr[APPSTACKSIZE/4];
vulong AppStack_irq[APPSTACKSIZE/4];
int t[8];

#define GPIO_MODULE 5
#define MASK_LED0   0x8000
#define MASK_LED1   0x2000
#define MASK_LED0_1 0xA000
#define MASK_SW0    0x1000
#define MASK_SW1    0x4000
#define MASK_SW2    BIT7

int main(void)
{
    //uchar* stringBuf = (uchar*)"Salut Bruno";
    //fb_print_string(stringBuf, 0, 0, 4);

    //Initialisation

    general_init();


    mmc1_init();




    /*
    //Etape 2
    char sizeCard[30];
    sprintf(sizeCard, "%ld", read_card_size());
    fb_print_string((uchar*)sizeCard, 0, 0, 4);
    uchar nameCard[6];
    read_productname(nameCard);
    fb_print_string(nameCard, 0, 30, 4);

    //Etape 3
    vulong block0[128] = {0};
    vulong block1[128] = {0};
    vulong block2[128] = {0};
    //vulong block3[128] = {0};
    mmchs_read_block(block0, 0);
    mmchs_read_block(block1, 1);
    mmchs_read_block(block2, 2);
    //mmchs_read_block(block3, 3);

    //Etape 4
    uchar multiBlock1[1*512] = {0};
    uchar multiBlock2[2*512] = {0};
    uchar multiBlock8[8*512] = {0};
    mmchs_read_multiple_block(multiBlock1, 0, 1);
    mmchs_read_multiple_block(multiBlock2, 0, 2);
    mmchs_read_multiple_block(multiBlock8, 0, 8);

    //Etape 5
    vulong block3[128] = {0};
    vulong blockResult[128] = {0};
    mmchs_read_block(block3, 3);
    mmchs_write_block(block1, 3);
    mmchs_read_block(blockResult, 3);
    mmchs_write_block(block3, 3);


    //Etape 6
    uchar multiBlock1Rd[1*512] = {0};
    uchar multiBlock2Rd[2*512] = {0};
    uchar multiBlock8Rd[8*512] = {0};
    uchar multiBlock1Wr[1*128] = {0x12, 0x13};
    uchar multiBlock2Wr[2*128] = {0};
    uchar multiBlock8Wr[8*128] = {0};
    mmchs_write_multiple_block(multiBlock1Wr, 0, 1);
    mmchs_write_multiple_block(multiBlock2Wr,0, 2);
    mmchs_write_multiple_block(multiBlock8Wr,0, 8);

    mmchs_read_multiple_block(multiBlock1Rd, 0, 1);
    mmchs_read_multiple_block(multiBlock2Rd, 0, 2);
    mmchs_read_multiple_block(multiBlock8Rd, 0, 8);
    */
    /*
    FATFS fs;
    char* test = "";

    f_mount(0, &fs);
    scan_files(test);
    f_mount(0, NULL);
    */

    //test_menu();

    FATFS fs;
    uchar buff[1990];
    buff[1989] = 0;
    f_mount(0, &fs);
    for(int i = 0; i < 1900; i++) {
        buff[i] = 'F';
    }
    create_file((char*)"test1900", buff, 1900);
    f_mount(0, NULL);

    while(!ReadInput(5, SW2));
    return(0);
}

